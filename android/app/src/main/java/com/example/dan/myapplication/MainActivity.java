package com.example.dan.myapplication;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import java.util.Locale;
import java.util.UUID;


public class MainActivity extends Activity implements BluetoothAdapter.LeScanCallback {

    private static final String TAG = MainActivity.class.getName();

    private static final UUID SERVICE_UUID = UUID.fromString("0000AFB2-C919-E493-E74B-A26912E081FA");
    private static final UUID CHARACTERISTIC_UUID = UUID.fromString("0300AFB2-C919-E493-E74B-A26912E081FA");

    // 10 bytes
//    private static final byte[] dummy_data = {
//            0x00,0x00,0x00,0x00,0x00,
//            0x00,0x00,0x00,0x00,0x00};

    // 20 bytes
    private static final byte[] dummy_data = {0x00,0x00,0x00,0x00,0x00,
                                              0x00,0x00,0x00,0x00,0x00,
                                              0x00,0x00,0x00,0x00,0x00,
                                              0x00,0x00,0x00,0x00,0x00};

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mBluetoothDevice;
    private BluetoothGatt mBluetoothGatt;

    private int mBytesToSentLeft = 1600;
    private int mBytesWritten = 0;
    private int mChuksSent = 0;

    private long mStartTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BluetoothManager bluetoothManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        Log.d(TAG,"Starting Le Scan...");
        mBluetoothAdapter.startLeScan(this);
    }

    @Override
    public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
        Log.d(TAG,"Found bluetooth device " + device.getAddress());


        mBluetoothAdapter.stopLeScan(this);

        mBluetoothDevice = device;

        Log.d(TAG,"connecting to bluetooth device " + device.getAddress());

        mBluetoothDevice.connectGatt(this,false,mGattCallback);

    }

    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if(BluetoothGatt.GATT_SUCCESS == status && BluetoothProfile.STATE_CONNECTED == newState) {
                Log.d(TAG,"connected to gatt server of device: " + gatt.getDevice().getAddress());
                mBluetoothGatt = gatt;
                Log.d(TAG,"discovering services...");
                mBluetoothGatt.discoverServices();
            } else {
                Log.d(TAG,"disconnected state=" + newState + " status=" + status);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if(BluetoothGatt.GATT_SUCCESS == status) {
                Log.d(TAG,"discovered services");
                Log.d(TAG,"Starting to write data..");
                mStartTime = System.currentTimeMillis();
                writeCharacterisitic(gatt);

            } else {
                Log.d(TAG,"error while trying to discover services");
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            mBytesWritten += dummy_data.length;
            mChuksSent++;
            Log.d(TAG, dummy_data.length + " bytes received by device. " + mChuksSent + " chunks, " + mBytesWritten  + " bytes.");

            mBytesToSentLeft -= dummy_data.length;
            if(mBytesToSentLeft > 0) {

                writeCharacterisitic(gatt);
            } else {
                long endTIme = System.currentTimeMillis();
                Log.d(TAG, String.format(Locale.ENGLISH,"Finished, total time: %d", (endTIme - mStartTime) * 1000));
            }
        }
    };

    private BluetoothGattCharacteristic getCharacteristic() {
        return mBluetoothGatt.getService(SERVICE_UUID).getCharacteristic(CHARACTERISTIC_UUID);
    }

    private void writeCharacterisitic(BluetoothGatt gatt) {
        Log.d(TAG,"sending 20 bytes");
        getCharacteristic().setValue(dummy_data);
        gatt.writeCharacteristic(getCharacteristic());
    }
}
