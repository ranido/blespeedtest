//
//  AppDelegate.h
//  speedtest
//
//  Created by Dan M on 2/5/15.
//  Copyright (c) 2015 Dan M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

