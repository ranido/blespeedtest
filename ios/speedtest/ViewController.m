//
//  ViewController.m
//  speedtest
//
//  Created by Dan M on 2/5/15.
//  Copyright (c) 2015 Dan M. All rights reserved.
//

#import "ViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface ViewController ()

@property (nonatomic, strong) CBCentralManager *CM;
@property (nonatomic, strong) NSMutableSet *ps;
@property (nonatomic, strong) CBService *s;
@property (nonatomic, strong) CBCharacteristic *writeChar1;
//@property (nonatomic, strong) CBCharacteristic *writeChar2;

@property (nonatomic, strong) NSDate *writeStartDate;
@property (nonatomic) int writeAmount;
@property (nonatomic) int bytesToSentLeft;
@property (nonatomic) int bytesToSendInOnePacket;
@property (nonatomic) int bytesWritten;
@property (nonatomic) int chunksSent;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
 
    _CM = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        
    _ps = [NSMutableSet set];
    _bytesToSentLeft = 1024 * 800;
    _bytesToSendInOnePacket = 10;
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    [_CM scanForPeripheralsWithServices:nil options:nil];
}


- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
    
}


- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals
{
    
}


- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    [_ps addObject:peripheral];
    [peripheral setDelegate:self];
    [_CM connectPeripheral:peripheral options:nil];
    
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    [peripheral discoverServices:nil];
}


- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    
}


- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    NSLog(@"%@", peripheral.services);
    for (CBService *service in peripheral.services)
    {
        if ([service.UUID.UUIDString isEqualToString:@"0000AFB2-C919-E493-E74B-A26912E081FA"])
        {
            [_CM stopScan];
            _s = service;
            break;
        }
    }
    
    if (_s)
    {
        [peripheral discoverCharacteristics:nil forService:_s];
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    NSLog(@"%@", service.characteristics);
    char b[]  = {0x00,0x00,0x00,0x00,0x00,
                0x00,0x00,0x00,0x00,0x00,
                0x00,0x00,0x00,0x00,0x00,
                0x00,0x00,0x00,0x00,0x00,
                0x00,0x00,0x00};
    
    for (CBCharacteristic *cha in service.characteristics) {
        if ([cha.UUID.UUIDString isEqualToString:@"0300AFB2-C919-E493-E74B-A26912E081FA"])
        {
            _writeChar1 = cha;
        }
    }

    if (_writeChar1)
    {
        _writeStartDate = [NSDate date];
        _writeAmount = 0;
        for (int x = 0; x < _bytesToSentLeft; x += _bytesToSendInOnePacket) {
            [peripheral writeValue:[NSData dataWithBytes:b length:_bytesToSendInOnePacket] forCharacteristic:_writeChar1 type:CBCharacteristicWriteWithResponse];
        }
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"ERROR IS %@", error);
    }
    else {
        _writeAmount++;
        _bytesWritten += _bytesToSendInOnePacket;
        _bytesToSentLeft -= _bytesToSendInOnePacket;
        _chunksSent++;
        
        NSLog(@"sent %d chunks, %d bytes", _chunksSent, _bytesWritten);
        if (_bytesToSentLeft <= 0) {
            NSDate *writeEndDate = [NSDate date];
            NSTimeInterval interval = [writeEndDate timeIntervalSinceDate:_writeStartDate];
            NSLog(@"Total time %f", interval);
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
